---
title: ts学习 --- 接口
date: 2022-06-11 11:40:45
tags: ts
summary: ts接口总结
categories: TypeScript
---

#### 接口

> `TypeScript`的核心原则之一就是对值具有的结构进行类型检查。接口的作用就是为这些类型命名和为你的代码定义'契约'或者可以理解为自定义的类型

#### 用法

##### 1. 普通用法

```
// 定义一个Info接口，所有Info类型的变量必须有name:string,age:number两个属性
interface Info {
    name: string;
    age: number;
}
```

##### 2. 可选属性

```
// 定义一个接口，其中name和age均为可选属性
interface Info {
    name?: string;
    age?: number;
}
```

##### 3. 只读属性

```
// 定义一个接口，其中name和age均为只读属性
// 赋值后不可再修改
interface Info {
    readonly name: string;
    readonly age: number;
}
let info:Info = {name: 'yanghong',age: 18}
info.age = 19 //error!!!
```

##### 4. 函数类型

> 通过接口的方式作为函数的类型来使用，为了使用接口表示函数类型，我们需要给接口定义一个**调用签名**
>
> > 调用签名：就像是一个只有参数列表和返回值类型的函数定义。参数列表里的每个参数都需要有类型

```
interface IsEqual {
	(a:number,b:number):boolean
}
const isEqual:IsEqual = function(a:number,b:number):boolean{
    return a===b
}
```

##### 5. 类类型

> 类的类型也可以通过接口来实现

```
interface SayName {
	sayName()
}
interface SayAge {
	sayAge()
}
class Info implements SayName,SayAge {
	sayName() {
		console.log('yh')
	},
    sayAge() {
        console.log(18)
    }
}
```

##### 6. 接口继承

> 接口可以继承，类似于类的继承

```
interface SayName {
	sayName()
}
interface SayAge {
	sayAge()
}
interface SayAll extents SayName,SayAge{}

class Info implements SayAll {
    sayName() {
		console.log('yh')
	},
    sayAge() {
        console.log(18)
    }
}
```

> 接口和接口之间叫继承（extends），类和接口之间是实现（implements）

